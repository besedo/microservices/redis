# Redis Container Image
FROM redis:6.2.13-alpine3.18

# Add User
RUN adduser ale;echo 'ale:123' | chpasswd
USER ale

# Exposes Port 6379
HEALTHCHECK CMD curl --fail http://localhost:6379 || exit 1
